//
//  ViewController.swift
//  Swift Scorekeeper
//
//  Created by Carissa on 9/11/17.
//  Copyright © 2017 Carissa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    // Team Red Text Field
    @IBOutlet weak var redText: UITextField!
    // Team Purple Text Field
    @IBOutlet weak var purpleText: UITextField!
    
    
    // Team Red Label
    @IBOutlet weak var RedLabel: UILabel!
    @IBAction func redStepper(_ sender: UIStepper) {
       self.RedLabel.text = Int(sender.value).description
    }
    
    
    // Team Purple Label
    @IBOutlet weak var PurpleLabel: UILabel!
    @IBAction func purpleStepper(_ sender: UIStepper) {
        self.PurpleLabel.text = Int(sender.value).description
    }
    

    @IBAction func Reset(_ sender: UIButton) {
        self.RedLabel.text = Int(0).description
        self.PurpleLabel.text = Int(0).description
        redText.text = String("")
        purpleText.text = String("")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        redText.becomeFirstResponder()
        purpleText.becomeFirstResponder()
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        
        view.addGestureRecognizer(tap)
        
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

